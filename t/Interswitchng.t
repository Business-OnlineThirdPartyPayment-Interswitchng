# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl eWay.t'

use Test;
BEGIN { plan tests => 5 };
use Business::OnlineThirdPartyPayment::Interswitchng;

# a test transaction
my ($tx, $txnum, $res);
ok($tx = new Business::OnlineThirdPartyPayment("Interswitchng"));
ok(
    $tx->content(
        type           => 'CC',
        login          => '87654321',
        password       => 'secret',
        action         =>  'Authorization Only',
        description    => 'Business::OnlineThirdPartyPayment test',
        amount         => '49.95',
        reference      => '1349',
    )
);
ok($tx->test_transaction(1));
ok($tx->submit());
ok($tx->is_success());

