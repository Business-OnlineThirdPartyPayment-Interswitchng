package Business::OnlineThirdPartyPayment::Interswitchng;

use strict;
use Carp;
use Business::OnlineThirdPartyPayment 3;
use Business::CreditCard;
use vars qw($VERSION @ISA $DEBUG);

@ISA = qw(Business::OnlineThirdPartyPayment);
$VERSION = '0.01';

$DEBUG = 0;

sub set_defaults {
  my $self = shift;

  $self->server('webpay.interswitchng.com') unless $self->server;
  $self->port('443') unless $self->port;
  $self->path('/webpay_pilot/purchase.aspx') unless $self->path;

}

sub reference {
    my ($self, $data) = @_;
    #$data->{TXNREF} || '';
    my @refkey = grep { /txnref/i } keys %{$data}; # @#$%#@%!
    my $refkey = $refkey[0] || '';  # please don't give me 2
    $data->{$refkey} || '';
}

sub submit {
    my($self) = @_;
    my %content = $self->content;

    my $action = lc($content{'action'});
    die 'Interswitchng only supports "Authorization Only" and '.
        '"Post Authorization" transactions'
      unless $action eq 'authorization only' || $action eq 'post authorization';

    my @required = qw( login amount reference );
    unless ($self->transaction_type() eq 'CC' ) {
      croak("Dummy can't handle transaction type: ".
            $self->transaction_type());
    }
    $self->required_fields(@required);

    $self->remap_fields (
                          login     => 'CADPID',
                          password  => 'MERTID',
                          reference => 'TXNREF',
                        );
    %content = $self->content;
    $content{AMT} = $content{amount} * 100;
    $content{TRANTYPE} = '00';

    my $url =
      "https://". $self->server().
      ($self->port != 443 ? ':'. $self->port() : ''). $self->path(). '?'.
      join( '&', map { "$_=$content{$_}" }
                 qw( CADPID MERTID TXNREF AMT TRANTYPE )
      );
    $self->popup_url( $url );

    $self->is_success(1);
}

1;
__END__

=head1 NAME

Business::OnlineThirdPartyPayment::Interswitchng - Interswitchng Webpay backend for Business::OnlineThirdPartyPayment

=head1 SYNOPSIS

  use Business::OnlineThirdPartyPayment;

  my $tx = new Business::OnlineThirdPartyPayment("Interswitchng");
  $tx->content(
      login          => '87654321', 
      action         => 'Normal Authorization',
      description    => 'Business::OnlinePayment test',
      amount         => '49.95',
      invoice_number => '100100',
  );
  $tx->submit();

  if($tx->is_success()) {
      print "Card processed successfully: ".$tx->authorization."\n";
  } else {
      print "Card was rejected: ".$tx->error_message."\n";
  }

=head1 DESCRIPTION

For detailed information see L<Business::OnlineThirdPartyPayment>.

=head1 NOTE

=head1 COMPATIBILITY

This module implements a payment gateway for Interswitchng's Webpay.

=head1 AUTHOR

Jeff Finucane <interswitchng@weasellips.com>

=head1 SEE ALSO

perl(1). L<Business::OnlineThirdPartyPayment>.

=cut

